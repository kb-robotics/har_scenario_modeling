import numpy as np

from sklearn import preprocessing
import pickle
import os
import json

import torch
from torch.autograd import Variable
import torch.optim as optim
import torch.nn.functional as F
from torch import nn
# from torch.nn import SmoothL1Loss

from torch.utils.data import Dataset, DataLoader
from torchvision import transforms

from ray import tune
from ray.tune import CLIReporter
from ray.tune.schedulers import ASHAScheduler

from common_utils.Dataset import Dataset
from common_utils.tcn_model import TCN
from common_utils.Dataset import PredictionDataset


class InferenceModel:
    '''
    @Given: the <model_dir>
    @Then: Load the Lable encoder, the model config and the model itsef
    '''
    def __init__(self, model_dir):
        self.device_ = self.__get_device() # cpu or gpu
        self.LE = self.__load_label_encoder(model_dir)
        self.__load_model(model_dir)
        self.is_multiclass_scenario_ = self.__is_multiclass_scenario(model_dir)
    
    def __load_label_encoder(self, model_dir):
        LE_f = os.path.join(model_dir, 'LE.pkl')
        f = open(LE_f,"rb")
        LE_loaded = pickle.load(f)
        f.close()
        return LE_loaded
    
    def __get_device(self):
        device = 'cpu'
        cuda=False
        if torch.cuda.is_available(): 
            device = 'cuda'
            cuda=True
#         print(cuda, device)
        return device

    def __load_TCN_config(self, model_dir):
        TCN_cfg_f = os.path.join(model_dir, 'TCN_cfg.json')

        with open(TCN_cfg_f) as json_file:
            TCN_cfg_loaded = json.load(json_file)
            
        return TCN_cfg_loaded
    
    def __is_multiclass_scenario(self, model_dir):
        TCN_cfg_loaded = self.__load_TCN_config(model_dir)
        is_multiclass_scenario = False
        
        if TCN_cfg_loaded['output_size'] > 1:
            is_multiclass_scenario = True
        
        return is_multiclass_scenario
            
        
    def __load_model(self, model_dir):
        #Load TCN_conf
        TCN_cfg_loaded = self.__load_TCN_config(model_dir)

        #Load TCN model
        self.model = TCN(TCN_cfg_loaded['input_channels'], TCN_cfg_loaded['output_size'], 
                           TCN_cfg_loaded['best_channel_sizes'], TCN_cfg_loaded['ksize'], 0).double()
        
        self.model.to(self.device_)

        model_path = os.path.join(model_dir,"best_tcn_model.pt")
        self.model.load_state_dict(torch.load(model_path))        
        
            
    def predict(self, data_in):
        
        pred_set_ind = PredictionDataset(data=data_in)
        pred_set_data_loader = DataLoader(pred_set_ind , batch_size=1, shuffle=False, num_workers=1)
        
        self.model.eval()
        preds = []
        with torch.no_grad():
            for data in pred_set_data_loader:
                if self.device_ == 'cuda':
                    data = data.cuda()

                data = data.view(-1, data_in.shape[2], data_in.shape[1])
                data = Variable(data)
                output = self.model(data.double())
                
                if self.is_multiclass_scenario_:
                    pred = output.data.max(1, keepdim=True)[1]
                    preds += [self.LE.inverse_transform(pred.cpu().numpy()[0])]
                else: #Binary scanrio
                    pred = torch.round(torch.sigmoid(output)).data
                    preds += [self.LE.inverse_transform(pred.squeeze(1).cpu().numpy().astype(int))]

        return np.array(preds)
    