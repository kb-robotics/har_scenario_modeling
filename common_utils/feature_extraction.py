import os
import glob
import json
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize
from sklearn.preprocessing import  MinMaxScaler


from scipy import signal

class ExtractFeatures:
    '''
    @Given: the folder <data_path> where the csv files correspond to diffferent pose estimations, and the config file which informes on the joints to use
    @Then: Extract features for the machine learning models to be trained on
    '''
    def __init__(self, data_path, har_config_f): #data_path is the directory where the csv files exist
        self.data_path = data_path
        
        self.joints_list, self.labels_list = self.__load_har_config(har_config_f)
#         = ['left_shoulder_y','left_shoulder_x','right_shoulder_y','right_shoulder_x', 'left_elbow_y','left_elbow_x','right_elbow_y','right_elbow_x', 'left_wrist_y','left_wrist_x','right_wrist_y','right_wrist_x']
#         self.labels_list = ['quality']
        self.data_df_list = self.__load_data()
    
    
    #Retrun relevant joint names and the label information from the config file
    def __load_har_config(self, har_config_f):
        with open(har_config_f) as json_file:
            har_json = json.load(json_file)
        return har_json['joints'], har_json['labels']
    
    
    #Extracts the features from one csv file, compiles them into a dataset <X> with labels <Y>
    def generate_features_single_video(self, csv_f, draw_plots=False):
        X = list()
        Y = list()
        
        df = self.__load_csv(csv_f)
        
        raw_features_df = df[self.joints_list]
        labels_df = df[self.labels_list]

        raw_features = raw_features_df.values #returns a numpy array
        features = self.__get_sampled_features(raw_features)
        if draw_plots:
            raw_features_df.plot()
            plt.show()
            plt.plot(features)
            plt.show()

        x = features
#         print(x)

        labels = labels_df.values
        y = labels[0] #All labels for an activity are same [We dont discard between static and moving activity]
#             print("label = ", y)
        X.append(x)
        Y.append(y)
        
        X = np.array(X)
        Y = np.array(Y)
        
        return X, Y
    
    #Extracts the features from the csv files, compiles them into a dataset <X> with labels <Y>
    def generate_features(self, draw_plots = False):
        X = list()
        Y = list()
        count = 0 
        for df in self.data_df_list:
#             print("COUNT:  ", count)
#             count = count + 1
#             print(df)
            raw_features_df = df[self.joints_list]
            labels_df = df[self.labels_list]
            
            raw_features = raw_features_df.values #returns a numpy array
            if draw_plots:
                raw_features_df.plot()
                plt.title("Raw features")
                plt.show()
            features = self.__get_sampled_features(raw_features)
            if draw_plots:               
                plt.plot(features)
                plt.title("Final features")
                plt.show()

            x = features
    #         print(x)

            labels = labels_df.values
            y = labels[0] #All labels for an activity are same [We dont discard between static and moving activity]
#             print("label = ", y)
            X.append(x)
            Y.append(y)
        X = np.array(X)
        Y = np.array(Y)
        return X, Y

    #Creates a combined data frame from all the invidual csv files, with wrist locations and object classes and labels
    def __load_data(self):
        files_dict = self.__get_files_dict()
    #     print(files_dict)
        return self.__create_data_df_list(files_dict)


    
    #Reads the floder and creates a key for each category and key is associated with all 
    # "*with_headers.csv" files for that specific category
    def __get_files_dict(self):
        activity_classes = os.listdir(self.data_path)
        files_dict = {}
        for activity in activity_classes:
            activity_dir = os.path.join(self.data_path,activity)
            files_dict[activity] = glob.glob(str(activity_dir)+'/**/*.csv', recursive=True) #MAGIC VARIABLE
        return files_dict

    #Creates a list of pandas dataframes from the cs files with the following columns:
    #'frame_index','left_wrist_y','left_wrist_x','right_wrist_y','right_wrist_x','quality'
    #here , apart from frame index is the timestampof an activity, wrist positions are features and quality is the label
    def __create_data_df_list(self, files_dict):
        data_df_list = list()
        for key in files_dict.keys():
            for csv_f in files_dict[key]:
                data_df = self.__load_csv(csv_f)

                data_df_list.append(data_df)

        return data_df_list
    
    def __load_csv(self, csv_f):
        
        df = pd.read_csv(csv_f)
        df_index = ['frame_index']
        data_df = df[df_index+self.joints_list+self.labels_list]

        data_df = data_df.set_index(df_index[0])

        return data_df
    
#     def __moving_average(self, x, w):
# #         print(x.shape)
#         return np.convolve(np.array(x), np.ones(w), mode='valid') / w

    #This function normalizes and uniformly divides the raw features into <num_smaples> regularly spaced time stamps 
    def __get_sampled_features(self, raw_features, num_samples = 150):
        
#         print(raw_features.shape)
        #Normalize between 0 and 1
    
#         scaler = MinMaxScaler()
#         scaler.fit(raw_features)
#         raw_features = scaler.transform(raw_features)
        
        for c in range(raw_features.shape[1]):
            raw_feature_min = np.min(raw_features[:,c])
            raw_features[:,c] -= raw_feature_min
            
            raw_feature_max = np.max(raw_features[:,c])
            raw_features[:,c] /= raw_feature_max
            raw_features[:,c] = signal.savgol_filter(raw_features[:,c], window_length=31, polyorder=2, deriv=1)

#         print(raw_features)
        
        idx = np.round(np.linspace(0, len(raw_features) - 1, num_samples)).astype(int)
        sampled_features = raw_features[idx]
        

        return sampled_features

