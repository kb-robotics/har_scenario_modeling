import torch.nn.functional as F
from torch import nn
from common_utils.tcn import TemporalConvNet


class TCN(nn.Module):
    def __init__(self, input_size, output_size, num_channels, kernel_size, dropout=0.):
        super(TCN, self).__init__()
#         print('0: ', input_size, output_size, num_channels, kernel_size, dropout)
        self.tcn = TemporalConvNet(input_size, num_channels, kernel_size=kernel_size, dropout=dropout)
        self.linear = nn.Linear(num_channels[-1], output_size)
#         self.init_weights() ANEESH: I want scaled output
        
    def init_weights(self):
        self.linear.weight.data.normal_(0, 0.001)

    def forward(self, inputs):
        """Inputs have to have dimension (N, C_in, L_in)"""
#         print("Inputs: ", inputs)
        
        y1 = self.tcn(inputs)  # input should have dimension (N, C, L)
#         print("inputs shape: ", inputs.shape)
#         print("y1 shape: ", y1.shape)
#         o = self.linear(F.relu(y1[:, :, -1]))
        o = self.linear(y1[:, :, -1])

#         print(" O shape: ", o.shape, o)
        return o
