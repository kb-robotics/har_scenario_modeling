import numpy as np
import torch
import torch.nn as nn

from torch.utils.data import Dataset

class Dataset(torch.utils.data.Dataset):
    'Characterizes a dataset for PyTorch'
    def __init__(self, data, labels):
        self.labels = labels
        self.data = data    
   
    def __len__(self):
        'total number of samples'
        return len(self.data)

    def __getitem__(self, idx):
        'get sample at index <idx>'
        # Select sample
        _X = self.data[idx, :].astype(np.float64)
        #print(_X.dtype)
        _y = self.labels[idx].astype(np.float64)

        
        return _X, _y
    
# Main differnce wrt to the "Dataset" class is that we have no knowledge of the label
class PredictionDataset(torch.utils.data.Dataset):
    'Characterizes a dataset for PyTorch'
    def __init__(self, data):
        self.data = data    
   
    def __len__(self):
        'total number of samples'
        return len(self.data)

    def __getitem__(self, idx):
        'get sample at index <idx>'
        # Select sample
        _X = self.data[idx, :].astype(np.float64)
        #print(_X.dtype)
        
        return _X