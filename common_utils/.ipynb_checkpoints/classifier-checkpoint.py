import numpy as np

from sklearn import preprocessing
import pickle
import os
import json

from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix

import matplotlib.pyplot as plt
from sklearn.preprocessing import  MinMaxScaler
from sklearn.ensemble import RandomForestClassifier

class Classifier:
    def __init__(self, model_dir):
        self.load(model_dir)
        
#         self.model = RandomForestModel()
        

    def load(self, model_dir):
        # save X_scaler_model and the ML model to disk
        
        le_f = os.path.join(model_dir, "label_encoder.pkl")
        self.le = pickle.load(open(le_f, 'rb'))
        
        X_scaler_model_f = os.path.join(model_dir, "X_scaler_model.pkl")
        self.X_scaler_model = pickle.load(open(X_scaler_model_f, 'rb'))
        
        model_f = os.path.join(model_dir, "model.pkl")
        self.model = pickle.load(open(model_f, 'rb'))
        

    def predict(self, x):
        x_scaled = self.X_scaler_model.transform(x)
        prediction = self.model.predict(x_scaled)
        return self.decode_labels(prediction)
    
    def predict_and_save(self, x, prediction_file):
        x_scaled = self.X_scaler_model.transform(x)
        prediction = self.model.predict(x_scaled)
        textual_prediction = self.decode_labels(prediction)
        
#         print(unique_predictions)
        prediction_dict = {}
        for ix, p in enumerate(textual_prediction):
            quality_location = p.split('_')
            quality = quality_location[0]
            location = quality_location[1]
            
            prediction_dict[quality] = location
        print(prediction_dict)
        
        with open(prediction_file, 'w') as fp:
            json.dump(prediction_dict, fp, indent=4)
            
        


    def get_class_names(self):
        unique_textual_labels = self.le.classes_
        return unique_textual_labels
        
    def get_num_classes(self):
        unique_textual_labels = self.le.classes_
        return len(unique_textual_labels)
    
            
    #Convert numerical labels to textual labels
    def decode_labels(self, numerical_labels):
        return self.le.inverse_transform(numerical_labels)
    


