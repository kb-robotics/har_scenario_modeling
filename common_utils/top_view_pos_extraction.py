import os
import glob
import numpy as np
import pandas as pd
import json
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize

from scipy import signal

class ExtractPositions:
    '''
    @Given: the folder <data_path> where the json files corresponding to diffferent hand placement x,y coordinates
    @Then: Extract those coordinates
    '''
    def __init__(self, data_path): #data_path is the directory where the json files exist
        self.data_path = data_path
        self.hand_xy = self.__load_data()
    

    #Load data from a single json file
    def load_data_single_video(self, json_fname):
        
        hand_xy = []
        
        with open(json_fname) as json_file:
            position_json = json.load(json_file)
            hand_xy = position_json['hand_xyc']
        
        hand_xy = np.array(hand_xy)
        return hand_xy

    #Creates a combined data frame from all the invidual json files, with wrist locations and object classes and labels
    def __load_data(self):
        files_dict = self.__get_files_dict()
        files_list = [v for v_list in files_dict.values() for v in v_list]
        
        hand_xy = []
        for position_f in files_list:
            with open(position_f) as json_file:
                position_json = json.load(json_file)
                hand_xy += [position_json['hand_xyc']]
        
        hand_xy = np.array(hand_xy)
        return hand_xy
                       
        

#         return self.__create_data_df_list(files_dict)


    
    #Reads the floder and creates a key for each category and key is associated with all 
    # "*.json" files for that specific category
    def __get_files_dict(self):
        
        activity_classes = os.listdir(self.data_path)
#         print(activity_classes)
        
        files_dict = {}
        for activity in activity_classes:
            activity_dir = os.path.join(self.data_path,activity)
#             print(activity_dir)
            files_dict[activity] = glob.glob(str(activity_dir)+'/**/*.json', recursive=True) #MAGIC VARIABLE
    
        return files_dict

    
