# Scenario 2 (Banana) predictions

{'ripe': array([[1071,  427],
       [1162,  355],
       [1107,  401],
       [ 414,  375],
       [1112,  393]]), 'unripe': array([[440, 414],
       [482, 374]])}
{'ripe': [1107, 393], 'unripe': [461, 394]}


# Scenario 3 (cucumbers) predictions
'cut': array([[443, 405],
       [468, 480],
       [433, 435],
       [422, 438]]), 'uncut': array([[1138,  336],
       [1120,  397],
       [1129,  419]])}
{'cut': [438, 436], 'uncut': [1129, 397]}

