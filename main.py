import argparse
import numpy as np
import os
import glob
import json
import time
from pathlib import Path

#Import the Classifier, feature extractor and position extractor
from common_utils.feature_extraction import ExtractFeatures
from common_utils.Dataset import Dataset, PredictionDataset
from common_utils.classifier import InferenceModel
from common_utils.top_view_pos_extraction import ExtractPositions




def get_args_parser():
    parser = argparse.ArgumentParser('HAR scenario classification', add_help=False)
    parser.add_argument('--har_scenario_cfg_file', type=str, help="The file which defines which joints are being used for har analysis")
    parser.add_argument('--data_dir', type=str, help="Directory where csv and json files for a specific scenario are stored for one or more video demonstrations")
    parser.add_argument('--model_dir', type=str, help="Directory where the models are stored for each scenario")
    parser.add_argument('--out_dir', type=str, help="Directory where resuting json from this module is stored")
    
    return parser

def predict_over_scenario_videos(args):
    model_dir = args.model_dir
    har_cfg_f = args.har_scenario_cfg_file
    data_dir = args.data_dir

    my_model = InferenceModel(model_dir)

    #Parse through all the videos
    results = {}
    for dname in os.listdir(data_dir):
        vid_dir = os.path.join(data_dir, dname)
        if os.path.isdir(vid_dir):
    
            sv_csv_f = glob.glob(str(vid_dir)+"/*.csv")
            tv_json_f = glob.glob(str(vid_dir)+"/*.json")

            if sv_csv_f and tv_json_f: #both files should exist
                fe = ExtractFeatures(vid_dir, har_cfg_f)
                vid_feats, vid_labels = fe.generate_features_single_video(sv_csv_f[0])#, draw_plots=True)
    
                tv_pos = ExtractPositions(data_dir)
                hand_pos = tv_pos.load_data_single_video(tv_json_f[0])

                quality = my_model.predict(vid_feats)
                results[vid_dir] = [quality, hand_pos]

#     print(results.values())
    #Cleanup class name (this is artificial, and will not be needed)
    for v in results.values():
        v[0] = str(v[0].squeeze()).split('_')[0]

#     print("Prediction results: ",  results)

    return results

def extract_hand_poses(results):
    #For given results, what are the hand positions
    hand_poses = {}
    print(results.values())
    
    for val in results.values():
        q=val[0]
        pos=val[1]
        try:

            hand_poses[q] = np.vstack( (hand_poses[q], pos))
        except:
            hand_poses[q] = pos
    
    print("hand_poses: \n", hand_poses)
    return hand_poses

def generate_scenario_definition(hand_poses):
    #Finally get the final hand poses
    scenario_dict = {}
    print(hand_poses)
    for q in hand_poses.keys():
        if hand_poses[q].ndim == 1: #If is only one position
            scenario_dict[q] = hand_poses[q].astype(int).tolist()
        else:
            scenario_dict[q] = np.median(hand_poses[q], axis=0).astype(int).tolist()

    print("Scenario dict: ", scenario_dict)
    return scenario_dict


def save_scenario_definition(scenario_dict, out_dir):
    scenario_f = out_dir+"/scenario_hand_pos.json"

    with open(scenario_f, 'w') as fp:
        json.dump(scenario_dict, fp, indent=4)

def main(args):

    scenario_prediction_results = predict_over_scenario_videos(args)
    hand_poses = extract_hand_poses(scenario_prediction_results)

    scenario_dict = generate_scenario_definition(hand_poses)

    #Save the results
    save_scenario_definition(scenario_dict, args.out_dir)


if __name__ == '__main__':
    parser = argparse.ArgumentParser('HAR scenario classification script', parents=[get_args_parser()])
    args = parser.parse_args()
    if args.out_dir:
        Path(args.out_dir).mkdir(parents=True, exist_ok=True)
    main(args)
