# har_scenario_modeling

Using the a sequence of human poses and object category that is being moved by the person, for a given Scenario, this module generates the model to predict the activity carried out by the person.